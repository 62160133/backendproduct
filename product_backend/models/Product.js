const mongoose = require('mongoose')
const { Schema } = mongoose
const productSchema = Schema({
  name: String,
  price: Number
})

module.exports = mongoose.module('Product', productSchema)
